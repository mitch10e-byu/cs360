#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include "net-client.h"

using namespace std;

int main(int argc, char **argv) {
	int option;
	int port = 8080;
	string host = "localhost";
	bool debug = false;

	while((option = getopt(argc, argv, "h:p:d")) != -1) {
		switch(option) {
			case 'h':
				host = optarg;
				break;
			case 'p':
				port = atoi(optarg);
				break;
			case 'd':
				debug = true;
				break;
			default:
				cout << "\033[31m" << "client [-h host -p port -d]" << "\033[0m" << endl;
				exit(EXIT_FAILURE);
		}
	}

	cout << "\033[31m" << "==INFO===\t" << "Starting Messaging Service Client" << "\033[0m" << endl;
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "Debugging Enabled" << "\033[0m" << endl;
	}

	InternetClient client = InternetClient(host, port);
	client.set_debug(debug);
	client.run();

}
