#pragma once

#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>


#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>

#include "message.h"

using namespace std;

class Server {
public:
	Server();
	~Server();
	void run();
	void set_debug(bool);

protected:
	virtual void create();
	virtual void close_socket();
	void serve();
	void handle(int);
	
	string get_header(int);
	string get_message(int, string);
	string get_request_length(int, int , string);
	bool send_response(int, string);
		
	string parse_request(string);
	string handle_get(string);
	string handle_list(string);
	string handle_put(string);
	string handle_reset();

	int _server;
	int _bufferLength;
	char* _buffer;
	bool debug;
	map<string, vector<Message> > messageList;

};

