#pragma once

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Message {
public:
	Message();
	~Message();
	
	string name();
	void name(string);
	
	string subject();
	void subject(string);
	
	int length();
	void length(int);
	
	string message();
	void message(string);

	string toString();
	string intToString(int);

private:
	string _name;
	string _subject;
	int _length;
	string _message;

};
