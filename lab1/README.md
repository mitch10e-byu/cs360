cs360
=====
Fall 2014

Lab 1: Messaging Server & Client (Not Threaded)

To Run: 
	Clone Repository
``` 
$ make
``` 

This will create the files necessary to run the project. Afterwards, run
``` 
$ ./msgd
```
to run the server and
``` 
$ ./msg
```
to start up the client. The testing script can be run by 
``` 
$ python messageTest.py
```
after the server is running.
