import argparse
from poller import Poller
from logger import Logger


DEFAULT_PORT = 4444


class Main:
    def __init__(self):
        self.logger = Logger()
        self.parse_arguments()

    def parse_arguments(self):
        parser = argparse.ArgumentParser(prog='Web Server', description='CS 360 Web Server (Event Driven)', add_help=True)
        parser.add_argument('-p', '--port', type=int, action='store', help='port the server will bind to',default=DEFAULT_PORT)
        parser.add_argument('-d', '--debug', action='store_true', help='enable [DEBUG] prints')
        parser.add_argument('-i', '--info', action='store_true', help='enable [INFO ] prints')
        self.args = parser.parse_args()
        if self.args.debug:
            self.logger.enable_debug()

    def run(self):
        server = Poller(self.args.port, self.args.debug, self.args.info)
        server.run()

if __name__ == "__main__":
    m = Main()
    m.parse_arguments()
    try:
        m.run()
    except KeyboardInterrupt:
        pass