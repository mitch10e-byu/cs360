import errno
import select
import socket
import os
import sys
import stat
import traceback
from time import *
from logger import Logger


CONFIG_FILE = "web.conf"
SERVER_NAME = "Hermes"

class Poller:
    def __init__(self, port, debug, info):
        print SERVER_NAME + " Server v1.0.0"
        self.logger = Logger()
        self.debug = debug
        if self.debug:
            self.logger.enable_debug()
        self.logger.debug("Web Server Debug Logging Enabled")
        self.info = info
        if self.info:
            self.logger.enable_info()
        self.logger.info("Web Server Info Logging Enabled")

        # Standard Server Parameters, Size set to 10x default lab size
        self.host = ""
        self.port = port
        self.size = 10240
        self.logger.debug("Server: " + self.host + ":" + str(self.port))

        # Web Server Configurations
        self.hosts = {}
        self.media = {}
        self.params = {}

        # Web Server Socket Support
        self.clients = {}
        self.markers = {}
        self.cache = {}
        self.timeout = 1
        self.mark_sweep_time_interval = 5
        # if self.debug:
            # Set interval to 20 to use telnet if only debug set
            # self.logger.debug("Mark & Sweep Interval set to 20")
            # self.mark_sweep_time_interval = 20

        # Web Server Request Handling
        self.request_types = ["GET", "POST", "PUT", "DELETE", "HEAD"]

        self.config()

        self.open_socket()

    def config(self):
        # self.logger.debug("Parsing Configuration File")
        conf = open(CONFIG_FILE, 'rb')
        for line in conf:
            if line[0] == '#':
                continue
            if len(line) > 1:
                line = line.split()
                if line[0] == "host":
                    hostname = line[1]
                    path = line[2]
                    if path[0] != "/":
                        path = os.getcwd() + "/" + path
                    self.hosts[hostname] = path

                elif line[0] == "media":
                    ext = line[1]
                    mime = line[2]
                    self.media[ext] = mime

                elif line[0] == "parameter":
                    param = line[1]
                    value = line[2]
                    self.params[param] = value

                else:
                    self.logger.err("Unknown Configuration Type" + line[0])
        conf.close()
        # Default Media type if not in web.conf
        self.media["default"] = "text/plain"
        self.parse_parameters()
        self.logger.print_dictionary(self.hosts)
        self.logger.print_dictionary(self.media)
        self.logger.print_dictionary(self.params)

    def parse_parameters(self):
        # self.logger.debug("Parsing parameters from configuration file")
        for param, value in self.params.iteritems():
            if param == "timeout":
                self.timeout = value

    def open_socket(self):
        try:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.server.bind((self.host, self.port))
            self.server.listen(5)
            self.server.setblocking(0)
        except socket.error, (value, message):
            if self.server:
                self.server.close()
            self.logger.err("Could not open socket: " + message)
            sys.exit(1)

    def close_socket(self, fd):
        # Remove client from all dictionaries and lists
        if fd in self.clients:
            self.clients[fd].close()
            del self.clients[fd]
        if fd in self.cache:
            del self.cache[fd]
        if fd in self.markers:
            del self.markers[fd]

    def run(self):
        # self.logger.debug("Starting Run (epoll)")
        self.poller = select.epoll()
        self.pollmask = select.EPOLLIN | select.EPOLLHUP | select.EPOLLERR
        self.poller.register(self.server, self.pollmask)

        start_time = time()
        while True:
            try:
                file_descriptors = self.poller.poll(timeout=1)
            except:
                return

            for (fd, event) in file_descriptors:
                if event & (select.POLLHUP | select.POLLERR):
                    self.handle_error(fd)
                    continue
                if fd == self.server.fileno():
                    self.handle_server()
                    continue
                self.markers[fd] = time()
                self.handle_client(fd)

            end_time = time()

            # Every 5 seconds, sweep through the clients to see if any are not sending data
            if (end_time - start_time) > self.mark_sweep_time_interval:
                self.logger.debug("--- Sweeping ---")

                #  create a list to hold clients that fail the sweep
                dead_clients = []
                for client in self.markers:
                    if float(end_time - self.markers[client]) > float(self.timeout):
                        # mark client as "dead"
                        dead_clients.append(client)

                self.logger.debug("Removing " + str(len(dead_clients)) + " clients")
                for client in dead_clients:
                    # close all connections to dead clients
                    self.close_socket(client)

                # reset timer and list for next mark and sweep
                start_time = time()

        # self.logger.debug("Ending Run (epoll)")

    def handle_error(self, fd):
        self.poller.unregister(fd)

        # If Server Error, Restart
        if fd == self.server.fileno():
            self.server.close()
            self.open_socket()
            self.poller.register(self.server, self.pollmask)
        else:
            self.close_socket(fd)

    def handle_server(self):
        # self.logger.debug("Handling Server")
        while True:
            try:
                (client, address) = self.server.accept()
            except socket.error, (value,message):
                # if socket blocks because no clients are available,
                # then return
                if value == errno.EAGAIN or errno.EWOULDBLOCK:
                    return
                print traceback.format_exc()
                sys.exit()
            # Set Non Blocking
            client.setblocking(0)
            self.clients[client.fileno()] = client
            self.markers[client.fileno()] = -1
            self.poller.register(client.fileno(), self.pollmask)
        # self.logger.debug("Handling Server Finished")

    def handle_client(self, fd):
        self.logger.debug("Handling Client")
        while True:
            try:
                data = self.clients[fd].recv(self.size)

                # add new data to fd in cache, or add fd to cache
                if fd in self.cache:
                    self.cache[fd] += data
                else:
                    self.cache[fd] = data

                # Parse Data, if end of headers found, send response (Only handling GET)
                if data:
                    self.logger.debug("DATA:\n" + data)
                    if "\r\n\r\n" in data:
                        (response, path) = self.parse_request(self.cache[fd])
                        self.send_response(fd, response, path)
                        # self.logger.debug("Cache Before: " + self.cache[fd])
                        del self.cache[fd]
                        break
                    else:
                        continue
                else:
                    self.logger.debug("No Data Sent")
                    self.logger.debug("Closing")
                    self.poller.unregister(fd)
                    self.close_socket(fd)
                    break
            except socket.error, (value, message):
                if value == errno.EWOULDBLOCK or value == errno.EAGAIN:
                    self.logger.debug("EWOULDBLOCK || EAGAIN handle_client()")
                    break
                    # self.logger.err(traceback.format_exc())
                    # sys.exit(1)

        self.logger.debug("Handling Client Finished")

    def parse_request(self, data):
        self.logger.debug("Parsing Request")
        response = ""
        path = ""
        host = ""

        request = data.split()
        headers = dict()
        for segment in data:
            if ": " in segment:
                try:
                    header = segment.split(": ")
                    headers[header[0]] = header[1]
                except:
                    response = self.error_response("400", "Bad Request")
                    return response, path

        try:
            request_type = request[0]
        except:
            self.logger.err("Not enough arguments")
            response = self.error_response("400", "Bad Request")
            return response, path
        if request_type not in self.request_types:
            self.logger.err(request_type + " is not a valid request")
            response = self.error_response("400", "Bad Request")
        else:
            if request_type != "GET":
                response = self.error_response("501", "Not Implemented")
            else:
                self.logger.debug("Valid Request Type :)")
                url = ""
                version = ""
                # if GET, architecture planned for adding HEAD
                if request_type == "GET":
                    try:
                        url = request[1]
                        version = request[2]
                    except:
                        self.logger.err("Not enough arguments")
                        response = self.error_response("400", "Bad Request")
                        return response, path
                    self.logger.debug("url: " + url)
                    self.logger.debug("version: " + version)
                    if "HTTP/1.1" in url:
                        # self.logger.debug("URL == HTTP/1.1")
                        response = self.error_response("400", "Bad Request")
                        return response, path
                    if "HTTP/1.1" not in version:
                        # self.logger.err("Bad Version")
                        response = self.error_response("400", "Bad Request")
                        return response, path
                    if url == "/":
                        url = "/index.html"
                    if "Host" in headers:
                        host = headers["Host"].split(":")[0]
                        # self.logger.debug("Host: " + host)
                    if host not in self.hosts:
                        host = self.hosts["default"]
                        # self.logger.debug("Setting Host to Default")
                    path = host + url
                    response = self.create_response(path)

        return response, path

    def create_response(self, path):
        # self.logger.debug("Creating Response for path: " + path)
        response = ""
        crlf = "\r\n"
        authorized = self.check_permission(path)
        if authorized is None:
            extension = path.split(".")[-1]
            if extension in self.media:
                extension = self.media[extension]
            else:
                extension = 'text/plain'
            self.logger.debug("File Extension: " + extension)

            response_head = "HTTP/1.1 200 OK" + crlf
            headers = "Date: " + self.timestamp() + crlf
            headers += "Server: " + SERVER_NAME + "/1.0.0 (Linux)" + crlf
            headers += "Content-Type: " + extension + crlf
            headers += "Content-Length: " + str(os.stat(path).st_size) + crlf
            headers += "Last-Modified: " + self.format_time_gmt(os.stat(path).st_mtime) + crlf
            headers += crlf
            response = response_head + headers
        else:
            response = authorized
        # self.logger.debug("End Create Response")
        return response

    def send_response(self, fd, response, path):
        self.logger.debug("Sending Response:\n" + response)
        while True:
            try:
                self.clients[fd].send(response)
                break
            except socket.error, e:
                if e.args[0] == errno.EWOULDBLOCK or e.args[0] == errno.EAGAIN:
                    self.logger.debug("EWOULDBLOCK || EAGAIN send_response()")
                    continue

        sent = 0
        if path and response.split()[1] == "200":
            path_file = open(path, "rb")
            while True:
                amount_read = path_file.read(self.size)
                if not amount_read:
                    break
                amount_sent = 0
                while amount_sent < len(amount_read):
                    try:
                        sent = self.clients[fd].send(amount_read[amount_sent:])
                    except socket.error, e:
                        if e.args[0] == errno.EWOULDBLOCK or e.args[0] == errno.EAGAIN:
                            self.logger.debug("EWOULDBLOCK || EAGAIN send_response() body")
                            continue
                    amount_sent += sent
            path_file.close()

        # self.logger.debug("End Send Response")

    def check_permission(self, path):
        self.logger.debug("Checking File Permission: " + str(file))
        if os.path.isfile(path):
            s = os.stat(path)
            # self.logger.debug("File Stat: " + str(s))

            # https://docs.python.org/2/library/stat.html#stat.S_IRGRP
            if s.st_mode & stat.S_IRGRP:
                return None
            else:
                return self.error_response("403", "Forbidden")
        else:
            return self.error_response("404", "Not Found")

    def error_response(self, code, message):
        self.logger.debug("Creating Response for Error: " + code + " " + message)
        crlf = "\r\n"
        body = "<html><body><h1>" + code + "</h1><br><p>" + message + "</p></body></html>"
        response_head = "HTTP/1.1 " + code + " " + message + crlf
        headers = "Date: " + self.timestamp() + crlf
        headers += "Server: " + SERVER_NAME + "/1.0.0 (Linux)" + crlf
        headers += "Content-Type: text/html" + crlf
        headers += "Content-Length: " + str(len(body)) + crlf + crlf
        error_response = response_head + headers + body + crlf + crlf
        return error_response

    def format_time_gmt(self, file_time):
        gmt = gmtime(file_time)
        gmt_format = '%a, %d %b %Y %H:%M:%S GMT'
        stamp = strftime(gmt_format, gmt)
        return stamp

    def timestamp(self):
        gmt = gmtime(time())
        gmt_format = '%a, %d %b %Y %H:%M:%S GMT'
        stamp = strftime(gmt_format, gmt)
        return stamp
