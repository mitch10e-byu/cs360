import argparse
import requests
import threading
from time import time


class DebugLogger:
    def __init__(self):
        self.debug = False

    def enable(self):
        self.debug = True

    def out(self, message):
        if self.debug:
            print "[DEBUG]\t" + message

    @staticmethod
    def i_out(message):
        print "[INFO] \t" + message


class Downloader:
    def __init__(self, threads, url, debug_logger):
        self.threads = threads
        self.url = url
        self.logger = debug_logger
        logger.out("Downloader Initialized")

    def download(self):
        file_size = long(requests.head(self.url).headers['content-length'])
        logger.out("File Size:\t%s" % file_size)
        thread_pool = []

        for i in range(0, self.threads):
            begin, end = self.partition(i, file_size)
            logger.out("Thread:\t%d" % i)
            logger.out("Range:\t%s-%s" % (begin, end))
            accelerator = Accelerator(self.url, begin, end, logger)
            thread_pool.append(accelerator)

        start_time = time()

        for thread in thread_pool:
            thread.start()

        # Get file name [-1] gets last split
        file_name = self.url.split("/")[-1]
        if file_name == "":
            file_name = "index.html"

        with open(file_name, "wb") as file:
            for thread in thread_pool:
                thread.join()
                file.write(thread.content)
            file.close()

        logger.out("File Name: %s" % file_name)
        end_time = time()
        total_time = end_time - start_time
        print "%s %s %s %s" % (self.url, self.threads, str(file_size), total_time)

    def partition(self, thread_number, file_size):
        partition = file_size / self.threads
        begin = thread_number * partition
        end = (thread_number + 1) * partition - 1
        if file_size - end < partition:
            end = file_size
        return begin, end


class Accelerator(threading.Thread):
    def __init__(self, url, start_index, end_index, log):
        threading.Thread.__init__(self)
        self.url = url
        self.logger = log
        self.start_index = start_index
        self.end_index = end_index
        self.content = None
        log.out("URL:\t%s" % self.url)
        log.out("Start Index:\t%s" % self.start_index)
        log.out("End Index:\t%s" % self.end_index)

    def run(self):
        logger.out("Accelerating Download")
        result = requests.get(
            self.url,
            headers={
                'Accept-Encoding': 'identity',
                'Range': 'bytes=%s-%s' % (self.start_index, self.end_index)
            }
        )
        self.content = result.content


def parse_options():
    parser = argparse.ArgumentParser(prog="downloadAccelerator", description="Download Accelerator", add_help=True)
    parser.add_argument('-n', type=int, action='store', help='Specify the number of threads to create', default=1)
    parser.add_argument('-d', action='store_true')
    parser.add_argument('url', help='give a url to a file to download')
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_options()
    logger = DebugLogger()
    if args.d:
        logger.enable()
    logger.out("Debugging Enabled")
    logger.out("URL: " + args.url)
    downloader = Downloader(args.n, args.url, logger)
    downloader.download()
