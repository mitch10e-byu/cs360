# http://docs.python-requests.org/en/latest/user/install/#install
import requests
import numpy
import matplotlib

matplotlib.use('Agg')
from time import *
from pylab import *
import os
# import subprocess

print "DELETING PREVIOUS DATA"
# os.system("analysis_data.txt")
# os.system("*.png")
os.system("rm web-*-*.txt")
sleep(1)

print "GATHERING DATA"

lighttpd = "http://localhost:55555/file333.txt"
web_server = "http://localhost:4444/file333.txt"
output_file = "analysis_data.txt"

repetitions = 100
light_times = []
server_times = []

duration = 30
server_port = 4444
light_port = 55555


def measure_request(file_path, time_array):
    start = time()
    r = requests.get(file_path)
    finish = time()
    elapsed = finish - start
    time_array.append(elapsed)


for x in range(0, 100):
    measure_request(lighttpd, light_times)
    measure_request(web_server, server_times)

server_average = numpy.mean(server_times)
light_average = numpy.mean(light_times)

file = open(output_file, "wb")
file.write("lighttpd: \n")
for item in light_times:
    file.write("%s " % item)
file.write("\n")
file.write("lighttpd average: " + str(light_average))
file.write("\n")
file.write("web server: \n")
for item in server_times:
    file.write("%s " % item)
file.write("\n")
file.write("web server average: " + str(server_average))
file.close()
print "DATA GATHERED"
print "BEGINNING ANALYSIS"

server_mu = 1 / float(server_average)
light_mu = 1 / float(light_average)

light_x = []
light_y = []
server_x = []
server_y = []


def analysis_graph_points(mu, is_light):
    smoothing = 100
    mu_diff = mu / smoothing
    for i in range(0, smoothing):
        lam = i * mu_diff
        x = lam / mu
        y = 1 / (mu - lam)
        if is_light:
            light_x.append(x)
            light_y.append(y)
        else:
            server_x.append(x)
            server_y.append(y)


analysis_graph_points(server_mu, False)
analysis_graph_points(light_mu, True)

clf()
plot(server_x, server_y)
xlabel("lambda / mu")
ylabel("1 / (mu - lambda)")
title("Theoretical Server Analysis")
savefig("server_theoretical.png")

clf()
plot(light_x, light_y)
xlabel("lambda / mu")
ylabel("1 / (mu - lambda)")
title("Theoretical lighttpd Analysis")
savefig("lighttpd_theoretical.png")

print "ANALYSIS GRAPHS GENERATED"
print "GENERATING LOADS"


light_load_data = []
server_load_data = []

server_workloads = []
light_workloads = []

interval = server_mu / 10
for i in range(0, 10):
    workload = (i * interval) + 1
    server_workloads.append(workload)
    output_file = "web-server-%s.txt" % int(workload)
    server_load_data.append(output_file)
    print "[file " + str(i + 1) + "]\t" + output_file
    os.system("python generator.py --port %s -l %s -d %s >> %s" % (server_port, int(workload), duration, output_file))
    # os.system("touch " + output_file)
    # process = subprocess.Popen(["python", "generator.py", "--port %s -l %s -d %s >> %s" % (server_port, int(workload), duration, output_file)])
    # process.wait()

interval = light_mu / 10
for i in range(0, 10):
    workload = (i * interval) + 1
    light_workloads.append(workload)
    output_file = "web-light-%s.txt" % int(workload)
    light_load_data.append(output_file)
    print "[file " + str(i + 1) + "]\t" + output_file
    os.system("python generator.py --port %s -l %s -d %s >> %s" % (light_port, int(workload), duration, output_file))
    # os.system("touch " + output_file)
    # process = subprocess.Popen(["python", "generator.py", "--port %s -l %s -d %s >> %s" % (server_port, int(workload), duration, output_file)])
    # process.wait()


print "GENERATE LOADS FINISHED"
# print "PAUSING 15 SECONDS TO WRITE FILES..."
# sleep(15)
print "CREATING BOXPLOTS"
server_box_x = []
server_box_y = []

light_box_x = []
light_box_y = []


def plotEvaluationGraph(x_values, y_values, server_name):
    clf()
    if server_name is "server":
        plot(server_x, server_y)
    else:
        plot(light_x, light_y)

    for i in range(0, len(x_values)):
        x_points = x_values[i]
        y_points = y_values[i]
        boxplot(y_points, widths=.05, positions=x_points, whis=2)
    xlim([0, 1])
    ylim([0, .1])
    xlabel('lambda / mu')
    ylabel('1 / (mu - lambda)')
    savefig(server_name + '_boxplot.png')


for data_file in server_load_data:
    x_wrapper = []
    y_wrapper = []

    try:
        f = open(data_file, "r")
        split_name = data_file.split("-")
        file_workload = split_name[2]
        file_workload = file_workload.split(".")
        lam = float(file_workload[0])
        x = lam / server_mu
        x_wrapper.append(x)
        server_box_x.append(x_wrapper)

        for line in f.readlines():
            line_split = line.split()
            if line_split[2] == '200':
                y = float(line_split[5])
                y_wrapper.append(y)
        server_box_y.append(y_wrapper)
        f.close()
    except IOError as e:
        print "IO Error: " + e.message
    plotEvaluationGraph(server_box_x, server_box_y, "server")

for data_file in light_load_data:
    x_wrapper = []
    y_wrapper = []
    try:
        f = open(data_file, "r")
        split_name = data_file.split("-")
        file_workload = split_name[2]
        file_workload = file_workload.split(".")
        lam = float(file_workload[0])
        # print lam
        x = lam / light_mu
        # print x
        x_wrapper.append(x)
        light_box_x.append(x_wrapper)

        for line in f.readlines():
            line_split = line.split()
            if line_split[2] == '200':
                y = float(line_split[5])
                y_wrapper.append(y)
        light_box_y.append(y_wrapper)
        f.close()
    except IOError as e:
        print "IO Error: " + e.message
    plotEvaluationGraph(light_box_x, light_box_y, "lighttpd")

print "DONE WITH BOXPLOTS"
print "QUEUING THEORY LAB FINISHED"