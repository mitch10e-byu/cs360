import math

print "Queuing Theory - CS 360 - Fall 2014"
print "Name: Mitchell Tenney"

print "[#1]\tYou are entering the Marriott Center to watch a basketball game. There is only one ticket line to purchase tickets. Each ticket purchase takes an average of 18 seconds. The average arrival rate is 3 persons/minute. Find the average length of queue and average waiting time in queue assuming M/M/1 queuing.\n"
print "SOLUTION:\n"

lam = 1 / float(20)
mu = 1 / float(18)
rho = lam / mu
print "lambda : 1 person / 20 seconds =\t\t" + str(lam)
print "mu     : 1 person / 18 seconds =\t\t" + str(mu)
print "rho    : lambda / mu =\t\t\t\t\t" + str(rho)

averageLength = float(pow(rho, 2)) / (1 - rho)
averageQueueWait = rho / (mu - lam)

# print "rho ^ 2  =\t\t" + str(pow(rho, 2))
# print "1 - rho =\t\t" + str(1 - rho)
# print "rho / (mu - lambda) = " + str(averageQueueWait)

print "\nAverage Length of Queue: " + str(averageLength) + " people"
print "Average Waiting Time in Queue: " + str(averageQueueWait) + " seconds"
print "\nEND SOLUTION FOR [#1]\n"

print "[#2]\tYou are now in line to get into the arena. There are 3 operating turnstiles with one ticket-taker each. On average it takes 3 seconds for a ticket-taker to process your ticket and allow entry. The average arrival rate is 40 persons/minute.\n\t\tFind the average length of queue, average waiting time in queue assuming M/M/N queuing. What is the probability of having exactly 5 people in the system?\n"
print "SOLUTION:\n"
servers = 3
lam = 2 / float(3)
mu = 1 / float(3)
rho = lam / mu

summation = 0
for i in range(0, servers) :
    part1 = pow(rho, i) / math.factorial(i)
    summation += part1
print "SUMMATION: " + str(summation)
part2 = pow(rho, servers) / (math.factorial(servers) * (1 - (rho / servers)))
p_not = 1 / (summation + part2)
    
print "servers (N)      = " + str(servers)
print "lambda  2 / 3    = " + str(lam)
print "mu      1 / 3    = " + str(mu)
print "rho              = " + str(rho)
print "P0               = " + str(p_not)

averageLength = ((p_not * pow(rho, (servers + 1))) / (math.factorial(servers) * servers)) * (1 / pow((1 - (rho / servers)), 2))
averageQueueWait = ((rho + averageLength) / lam) - (1 / mu)
p_five = (pow(rho, 5) * p_not) / (pow(servers, (5 - servers)) * math.factorial(servers))

print "\nAverage Length of Queue: " + str(averageLength) + " people"
print "Average Waiting Time in Queue: " + str(averageQueueWait) + " seconds"
print "Probability of having 5 people in the system: " + str(p_five) + " --> " + str(p_five * 100) + "%"
print "\nEND SOLUTION FOR [#2]\n"

print "[#3]\tYou are now inside the arena. They are passing out Cosmo the Cougar bobblehead dolls as a free giveaway. There is only one person passing these out and a line has formed behind her. It takes her exactly 6 seconds to hand out a doll and the lam rate averages 9 people/minute.\n\t\tFind the average length of queue, average waiting time in queue, and average time spent in the system assuming M/D/1 queuing.\n"
print "SOLUTION:\n"

lam = 9 / float(60)
mu = 1 / float(6)
rho = lam / mu

print "lambda  9 / 60   = " + str(lam)
print "mu      1 / 6    = " + str(mu)
print "rho              = " + str(rho)

averageLength = pow(rho, 2) / (2 * (1 - rho))
averageQueueWait = (1 / (2 * mu)) * (rho / (1 - rho))
averageSystemTime = (1 / (2 * mu)) * ((2 - rho) / (1 - rho))

print "\nAverage Length of Queue: " + str(averageLength) + " people"
print "Average Waiting Time in Queue: " + str(averageQueueWait) + " seconds"
print "Average Time in System: " + str(averageSystemTime) + " seconds"

print "\nEND SOLUTION FOR [#3]\n"