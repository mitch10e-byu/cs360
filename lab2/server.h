#pragma once

#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>

#include "message.h"
#include "mailbox.h"
#include "buffer.h"

#include <pthread.h>
#include <semaphore.h>

using namespace std;

struct ThreadSafe {
	Mailbox mailbox;
	Buffer clients;
	int threadID;
	sem_t lock;
};

class Server {
public:
	Server();
	~Server();
	void run();
	void set_debug(bool);

protected:
	virtual void create();
	virtual void close_socket();
	void serve();
	static void* handleClient(void*);
	void make_threads();
	
	ThreadSafe* sharedData;
	vector<pthread_t*> threads;
	int NUM_THREADS;
	int _server;
	bool debug;
	
};

