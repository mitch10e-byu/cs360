#include "client.h"

Client::Client() {
	_bufferLength = 1024;
	_buffer = new char[_bufferLength + 1];
	debug = false;
}

Client::~Client() {}

void Client::run() {
	create();
	execute();
}

void Client::create() {}

void Client::close_socket() {}

void Client::execute() {
	string input;
	string request = "";
	string response = "";

	cout << "% ";
	while(getline(cin, input)) {
		request = parse_command(input);
		if(debug){
			cout << "\033[34m" << "==DEBUG==\t" << "Request: " << request << "\033[0m" << endl;
		}
		if(request.size() == 0) {
			cout << "% ";
			continue;
		} else if(request == "invalid input") {
			cout << "% ";
			continue;
		}
		bool success = send_request(request);
		if(not success) {
			break;
		}
		success = get_response();
		if(not success) {
			break;
		}
		cout << "% ";
	}
	close_socket();
}

bool Client::send_request(string request) {
	const char* pointer = request.c_str();
	int nleft = request.length();
	int nwritten;

	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "send_request: " << request << "\033[0m" << endl;
	}

	while (nleft) {
		if((nwritten = send(_server, pointer, nleft, 0)) < 0) {
			if(errno == EINTR) {
				continue;
			} else {
				perror("write");
				return false;
			}
		} else if (nwritten == 0) {
			return false;
		}
		nleft -= nwritten;
		pointer += nwritten;
	}
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "returning true" << "\033[0m" << endl;
	}
	return true;
}

bool Client::get_response() {
	string response = "";
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "Entering Get Response" << "\033[0m" << endl;
	}
	while (response.find("\n") == string::npos) {
		int nread = recv(_server, _buffer, 1024, 0);
		if(debug){
			cout << "\033[34m" << "==DEBUG==\t" << "nread: " << nread << "\033[0m" << endl;
		}
		if(nread < 0) {
			if(errno == EINTR) {
				continue;
			} else {
				return false;
			}
		} else if (nread == 0) {
			return false;
		}
		response.append(_buffer, nread);
	}

	if(response.substr(0, 8) == "message ") {
		stringstream ss;
		ss.str(response);
		string subject;
		string message = response.substr(response.find("\n") + 1);
		string command;
		string length;
		ss >> command >> subject >> length;
		response = subject + "\n" + message + "\n";
	} else if(response.substr(0, 4) == "list") {
		response = response.substr(response.find_first_of("\n") + 1);
	}

	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "response: " << response << "\033[0m" << endl;
	}
	if(response.find("OK") == string::npos) {
		cout << response;
	}
	return true;
}

void Client::set_debug(bool d) {
	debug = d;
}

string Client::parse_command(string input) {
	string result = "";
	stringstream ss;
	ss.str(input);
	string command;
	ss >> command;

	if(command == "send") {
		result = prepare_send(input);
	} else if(command == "list") {
		result = prepare_list(input);
	} else if(command == "read") {
		result = prepare_read(input);
	} else if(command == "quit") {
		if(debug){
			cout << "\033[34m" << "==DEBUG==\t" << "Exiting Client" << "\033[0m" << endl;
		}
		exit(0);
	} else if(command == "") {
		result = "";
	} else {
		cout << command << " is not a valid command.\n";
	}
	if(result == "invalid input") {
		cout << input << " is not valid\n";
	}

	return result;
}

string Client::prepare_send(string input) {
	stringstream ss;
	ss.str(input);
	string command;
	string name;
	string subject;
	ss >> command >> name >> subject;

	// Validate
	string invalid = "invalid input";
	if(name.empty()) {
		return invalid;
	} else if(subject.empty()) {
		return invalid;
	}

	cout << "- Type your message. End with a blank line -\n";
	string message = get_message();
	int length = message.size();
	stringstream result;
	result << "put " << name << " " << subject << " " << length << "\n" << message;
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "prepare_send: " << result.str() << "\033[0m" << endl;
	}
	
	return result.str();
}

string Client::get_message() {
	string result = "";
	string input = "";
	
	while(getline(cin, input)) {
		if(input.size() > 0) {
			result += input + "\n";
		} else {
			break;
		}
	}
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "message: " << result << "\033[0m" << endl;
	}
	return result;
}

string Client::prepare_list(string input) {
	stringstream ss;
	ss.str(input);
	string command;
	string name;
	ss >> command >> name;

	// Validate
	string invalid = "invalid input";
	if(name.empty()) {
		return invalid;
	}

	return ss.str() + "\n";
}

string Client::prepare_read(string input) {
	stringstream ss;
	ss.str(input);
	string command;
	string name;
	string index_string;
	ss >> command >> name >> index_string;

	string invalid = "invalid input";
	// Validate
	if(name.empty()) {
		return invalid;
	} else if(index_string.empty()) {
		return invalid;
	}

	int index;
	if(isdigit(index_string[0])) {
		index = atoi(index_string.c_str());
	} else {
		return invalid;
	}

	
	stringstream result;
	result << "get " << name << " " << index << "\n";
	
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "prepare_read: " << result.str() << "\033[0m" << endl;
	}
	return result.str();
}






