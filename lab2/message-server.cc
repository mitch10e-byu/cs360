#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include "net-server.h"

using namespace std;

int main(int argc, char **argv) {
	int option;
	int port = 8080;
	bool debug = false;
	
	while((option = getopt(argc, argv, "p:d")) != -1) {
		switch(option) {
			case 'p':
				port = atoi(optarg);
				break;
			case 'd':
				debug = true;
				break;
			default:
				cout << "\033[31m" << "msgd [-p port -d]" << "\033[0m" << endl;
				exit(EXIT_FAILURE);
		}
	}
	
	if(debug){
		cout << "\033[31m" << "==INFO===\t" << "Starting Message Service Daemon" << "\033[0m" << endl;
		cout << "\033[34m" << "==DEBUG==\t" << "Debugging Enabled" << "\033[0m" << endl;
	}

	InternetServer server = InternetServer(port);
	server.set_debug(debug);
	server.run();

}

