CS 360 Fall 2014 Lab 2: Threaded Messaging Server 
=====

To Run: 
	Clone Repository
``` 
$ make
``` 

This will create the files necessary to run the project. Afterwards, run
``` 
$ ./msgd
```
to run the server. The default port is 8080. The testing script can be run by 
``` 
$ python messageLoadTest.py -p [port] -t [#ofthreads] -r [#ofrepititions]
```
after the server is running.

PASS OFF INFORMATION FOR TA'S
=====
DATA STRUCTURES
	
	Buffer - buffer.h, buffer.cc
		Used to buffer the incoming clients - This is a Thread Safe queue<int>

	Mailbox - mailbox.h, mailbox.cc
		Used to safely store messages - This is a Thread Safe map<string, vector<Message> >

	
NOTE: I found a bug in my client that i have fixed for this lab. I accidentally was parsing my client read() using a stringstream and it was only getting the first word.

