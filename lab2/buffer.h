#pragma once

#include <iostream>
#include <string>
#include <queue>
#include <semaphore.h>

using namespace std;

class Buffer {
public:
	Buffer();
	void add(int);
	int pop();

protected:
	queue<int> clients;
	sem_t s;
	sem_t n;
	sem_t e;

};
