#include "net-client.h"

InternetClient::InternetClient(string host, int port) {
	_host = host;
	_port = port;
	debug = false;
}

InternetClient::~InternetClient() {}

void InternetClient::create() {
	struct sockaddr_in server_address;

	// use DNS to get IP Address
	struct hostent *hostEntry;
	hostEntry = gethostbyname(_host.c_str());
	if(!hostEntry) {
		cout << "\033[31m" << "==INFO===\t" << "No such host name: " << _host << "\033[0m" << endl;
		exit(-1);
	}

	memset(&server_address, 0, sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(_port);
	memcpy(&server_address.sin_addr, hostEntry->h_addr_list[0], hostEntry->h_length);

	_server = socket(PF_INET, SOCK_STREAM, 0);
	if(!_server) {
		perror("socket");
		exit(-1);
	}

	if(connect(_server, (const struct sockaddr *)&server_address, sizeof(server_address)) < 0) {
		perror("connect");
		exit(-1);
	}
}

void InternetClient::close_socket() {
	close(_server);
}

















