#pragma once

#include <netinet/in.h>
#include "server.h"

class InternetServer : public Server {
public:
	InternetServer(int);
	~InternetServer();

protected:
	void create();
	void close_socket();

private:
	int _port;

};






























