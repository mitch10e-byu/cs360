#include "buffer.h"

Buffer::Buffer() {
	int BUFFER_SIZE = 1000;

	clients = queue<int>();
	sem_init(&s, 0, 1);
	sem_init(&n, 0, 0);
	sem_init(&e, 0, BUFFER_SIZE);
}

void Buffer::add(int client) {
	sem_wait(&e);
	sem_wait(&s);
	clients.push(client);
	// Uncomment these and run the messageLoadTest.py with large numbers to watch the queue grow.
//	cout << "\nADD\tClients Size: " << clients.size();
	sem_post(&s);
	sem_post(&n);
}

int Buffer::pop() {
	sem_wait(&n);
	sem_wait(&s);
	int client = clients.front();
	clients.pop();
//	cout << "\nPOP\tClients Size: " << clients.size();
	sem_post(&s);
	sem_post(&e);
	return client;
}


