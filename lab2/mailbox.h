#pragma once

#include "message.h"

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <semaphore.h>

using namespace std;

class Mailbox {
public:
	Mailbox();
	void send(Message&);
	string list(string);
	Message read(string, int);
	vector<Message> getMessages(string);
	void clear();
	bool hasUser(string);

protected:
	map<string, vector<Message> > mailbox;
	sem_t lock;

};
