#include "handler.h"

Handler::Handler(ThreadSafe* sharedData) {
	// Assign Handler Pointers to point to sharedData for all threads
	this->mailbox = &sharedData->mailbox;
	this->clients = &sharedData->clients;
	this->lock = &sharedData->lock;
	this->threadID = sharedData->threadID;
	
	_bufferLength = 1024;
	_buffer = new char[_bufferLength + 1];
	debug = false;
	handle();
}

Handler::~Handler() {
	delete _buffer;
}

void Handler::handle() {
	int client;
	while(true) {
		// Get next client from Buffer
		client = clients->pop();
		handle(client);
		close(client);
	}
}

void Handler::handle(int client) {
	// This line checked to make sure the same thread was not doing all the work.
//	cout << "ThreadID: " << threadID << endl;
	while(true) {
		string request = get_header(client);
		if(request.empty()) {
			break;
		}
		// If put, need to get rest of message
		request = get_message(client, request);
		string response = parse_request(request);
		bool success = send_response(client, response);	
		if(not success) {
			break;
		}
	}
}



string Handler::get_header(int client) {
	string request = "";
	// Read until a newline, as all commands send at least one
	while (request.find("\n") == string::npos) {
		int nread = recv(client, _buffer, 1024, 0);
		if(debug){
			cout << "\033[34m" << "==DEBUG==\t" << "recieved(header): " << nread << "\033[0m" << endl;
		}
		if (nread < 0) {
			if (errno == EINTR) {
				continue;
			} else {
				return "";
			}
		} else if (nread == 0) {
			return "";
		}
		request.append(_buffer, nread);
	}
	return request;
}

string Handler::get_message(int client, string request){
	// Need to get rest of message after put
	string header = request.substr(0, request.find("\n"));
	int index = request.find("put");
	if(index != 0) {
		return request;
	}
	// Go to last space in header, next part is length
	string l = header.substr(header.find_last_of(" "));
	int length = atoi(l.c_str());
	
	// The rest of the message is everything after newline
	string message = request.substr(request.find("\n") + 1);
	if(message.length() >= length)
		return request;

	request = header + "\n" + get_request_length(client, length, message);
	return request;
}

string Handler::get_request_length(int client, int length, string request) {
	while (request.length() < length) {
		int nread = recv(client, _buffer, 1024, 0);
		if(debug){
			cout << "\033[34m" << "==DEBUG==\t" << "recieved(length): " << nread << "\033[0m" << endl;
		}
		if (nread < 0) {
			if (errno == EINTR) {
				continue;
			} else {
				return "";
			}
		} else if (nread == 0) {
			return "";
		}
		request.append(_buffer, nread);
	}
	return request;
}

bool Handler::send_response(int client, string response) {
	const char* pointer = response.c_str();
	int nleft = response.length();
	int nwritten;
	while(nleft) {
		if(debug){
			cout << "\033[34m" << "==DEBUG==\t" << "response(send): " << response << "\033[0m" << endl;
		}
		if((nwritten = send(client, pointer, nleft, 0)) < 0) {
			if(errno == EINTR) {
				continue;
			} else {
				return false;
			}
		} else if(nwritten == 0) {
			return false;
		}
		nleft -= nwritten;
		pointer += nwritten;
	}
	return true;
}

string Handler::parse_request(string request) {
	stringstream ss;
	ss.str(request);
	string command = "";
	ss >> command;
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "Command: " << command << "\033[0m" << endl;
	}
	string response = "";
	if(command == "get") {
		response = handle_get(request);
	} else if(command == "list") {
		response = handle_list(request);
	} else if(command == "put") {
		response = handle_put(request);
	} else if(command == "reset") {
		response = handle_reset();
	} else {
		response = "error Invalid Command: " + command + "\n";
	}
	return response;
}


string Handler::handle_get(string request) {
	stringstream ss;
	ss.str(request);
	string command;
	string name;
	string index_string;
	ss >> command >> name >> index_string;
	
	if(name.empty() || index_string.empty()) {
		return "error invalid request\n";
	}
	int index = atoi(index_string.c_str());



	if(!mailbox->hasUser(name) || index > mailbox->getMessages(name).size()) {
		return "error No Message At That Index\n";
	}

	stringstream response;
	response << "message " << mailbox->getMessages(name)[index-1].subject() << " "
			 << mailbox->getMessages(name)[index-1].message().size()
			 << "\n" << mailbox->getMessages(name)[index-1].message();
	return response.str();
}

string Handler::handle_list(string request) {
	stringstream ss;
	ss.str(request);
	string command;
	string name;
	
	ss >> command >> name;
	
	if(name.empty()) {
		return "error invalid request\n";
	}

	string emptyList = "list 0\n";
	if(!mailbox->hasUser(name)) {
		return emptyList;
	}
	
	return mailbox->list(name);
}

string Handler::handle_put(string request) {
	stringstream ss;
	ss.str(request);
	string command;
	string name;
	string subject;
	string length_string;
	string message;

	
	ss >> command >> name >> subject >> length_string;
	
	
	if(name.empty() || subject.empty() || length_string.empty()) {
		return "error invalid request\n";
	}
	
	// Length of all values in headers + 4 characters in between (3 space, 1 newline)
	int headerLength = command.size() + name.size() + subject.size() + length_string.size() + 4;
	message = request.substr(headerLength);
	int length = atoi(length_string.c_str());
	
	
	Message m = Message();
	m.name(name);
	m.subject(subject);
	m.length(message.size());
	m.message(message);
	
	mailbox->send(m);
	return "OK\n";
}

string Handler::handle_reset() {
	mailbox->clear();
	return "OK\n";
}
