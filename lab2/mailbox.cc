#include "mailbox.h"
#include <sstream>

Mailbox::Mailbox() {
	mailbox = map<string, vector<Message> >();
	sem_init(&lock, 0, 1);
}

void Mailbox::send(Message& message) {
	sem_wait(&lock);
	mailbox[message.name()].push_back(message);
	sem_post(&lock);
}

string Mailbox::list(string user) {
	sem_wait(&lock);
	stringstream ss;
	ss.str("");
	vector<Message> messages = mailbox[user];
	ss << "list " << messages.size() << endl;
	for(int i = 0; i < messages.size(); i++) {
		ss << (i + 1) << " " << messages[i].subject() << endl;
	}
	sem_post(&lock);
	string result = ss.str();
	return result;
}

Message Mailbox::read(string user, int index) {
	sem_wait(&lock);
	Message message = mailbox[user][index];
	sem_post(&lock);
	return message;
}

vector<Message> Mailbox::getMessages(string user) {
	sem_wait(&lock);
	vector<Message> messages = mailbox[user];
	sem_post(&lock);
	return messages;
}

void Mailbox::clear() {
	sem_wait(&lock);
	mailbox.clear();
	sem_post(&lock);
}

bool Mailbox::hasUser(string user) {
	sem_wait(&lock);
	bool count = mailbox.count(user) != 0;
	sem_post(&lock);
	return count;
}
