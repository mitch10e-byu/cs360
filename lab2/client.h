#pragma once

#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Client {
public:
	Client();
	~Client();
	void run();
	void set_debug(bool);

protected:
	int _server;
	int _bufferLength;
	char* _buffer;
	bool debug;

	virtual void create();
	virtual void close_socket();
	void execute();
	bool send_request(string);
	bool get_response();

	string parse_command(string);	
	string prepare_send(string);
	string get_message();
	string prepare_list(string);
	string prepare_read(string);


};





































