#include "message.h"

Message::Message() {
	_name = "";
	_subject = "";
	_length = 0;
	_message = "";
}

Message::~Message() {}

string Message::name() {
	return _name;
}

void Message::name(string name) {
	_name = name;
}

string Message::subject() {
	return _subject;
}

void Message::subject(string subject) {
	_subject = subject;
}

int Message::length() {
	return _length;
}

void Message::length(int length) {
	_length = length;
}

string Message::message() {
	return _message;
}

void Message::message(string message) {
	_message = message;
}

string Message::toString() {
	string result = _subject += " ";
	result += intToString(_length) + "\n";
	result += _message;
	return result;
}

string Message::intToString(int length) {
	stringstream ss;
	ss << length;
	return ss.str();
}

