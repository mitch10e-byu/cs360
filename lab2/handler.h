#pragma once

#include "server.h"

using namespace std;

class Handler {
public:
	Handler(ThreadSafe*);
	~Handler();
	void set_debug(bool);
	
private:
	void handle();
	void handle(int);

	string get_header(int);
	string get_message(int, string);
	string get_request_length(int, int , string);
	bool send_response(int, string);
		
	string parse_request(string);
	string handle_get(string);
	string handle_list(string);
	string handle_put(string);
	string handle_reset();
	
	int _bufferLength;
	char* _buffer;
	bool debug;
	Mailbox* mailbox;
	Buffer* clients;
	sem_t* lock;
	int threadID;
	
};
