#include "net-server.h"

InternetServer::InternetServer(int port) {
	_port = port;
	debug = false;
}

InternetServer::~InternetServer() {}

void InternetServer::create() {
	struct sockaddr_in server_address;

	memset(&server_address, 0, sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(_port);
	server_address.sin_addr.s_addr = INADDR_ANY;

	_server = socket(PF_INET, SOCK_STREAM, 0);
	if(!_server) {
		perror("socket");
		exit(-1);
	}

	int reuse = 1;
	if(setsockopt(_server, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0) {
		perror("setsockopt");
		exit(-1);
	}

	if(bind(_server, (const struct sockaddr *)&server_address, sizeof(server_address)) < 0) {
		perror("bind");
		exit(-1);
	}

	if(listen(_server, SOMAXCONN) < 0) {
		perror("listen");
		exit(-1);
	}
}

void InternetServer::close_socket() {
	close(_server);
}




























