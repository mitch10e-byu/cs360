#include "server.h"
#include "handler.h"

Server::Server() {
	debug = false;

	sharedData = new ThreadSafe;
	sharedData->mailbox = Mailbox();
	sharedData->clients = Buffer();
	sem_init(&sharedData->lock, 0, 1);
	sharedData->threadID = 0;
	
	NUM_THREADS = 10;
}

Server::~Server() {
	if(debug){
		std::cout << "\033[34m" << "==DEBUG==\t" << "Deleting Server" << "\033[0m" << std::endl;
	}
}

void Server::run() {
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "Running Server" << "\033[0m" << endl;
	}
	create();
	make_threads();
	serve();
}

void Server::set_debug(bool d) {
	debug = d;
}

void Server::create() {}
void Server::close_socket() {}


void Server::serve() {
	int client;
	struct sockaddr_in client_addr;
	socklen_t clientlen = sizeof(client_addr);

	// Add clients to a queue to wait to be handled.
	while((client = accept(_server, (struct sockaddr *)&client_addr, &clientlen)) > 0) {
		sharedData->clients.add(client);
	}
	close_socket();
}

void* Server::handleClient(void* arg) {
	// Cast arg to be a ThreadSafe pointer, pass on to Handler(ThreadSafe*)
	ThreadSafe* data;
	data = (ThreadSafe *) arg;
	data->threadID++;
	Handler((ThreadSafe *) arg);
	// Release the thread
	pthread_exit(0);
}

void Server::make_threads() {
	// NOTE FOR TA: This is where the TheadPool is created.
	for(int i = 0; i < NUM_THREADS; i++) {
		pthread_t* thread = new pthread_t;
		pthread_create(thread, NULL, Server::handleClient, (void *) sharedData);
		threads.push_back(thread);
	}
	if(debug){
		cout << "\033[34m" << "==DEBUG==\t" << "Number of Threads Created: " << threads.size() << "\033[0m" << endl;
	}
}


