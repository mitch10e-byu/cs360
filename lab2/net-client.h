#pragma once

#include <netinet/in.h>
#include "client.h"

class InternetClient : public Client {
public:
	InternetClient(string, int);
	~InternetClient();

protected:
	void create();
	void close_socket();

private:
	string _host;
	int _port;

};















